//
//  UsersController.swift
//  TawkGithubUsers
//
//  Created by Luigi Guevarra on 7/27/20.
//  Copyright © 2020 Luigi Guevarra. All rights reserved.
//

import UIKit
import CoreData

class UsersController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    var selectedIndex = 0
    
    var tblVwUsers = UITableView()
    var searchBar = UISearchBar()
    var lblNetworkStatus = UILabel()
    
    var userViewModels: [UserViewModel] = [UserViewModel]()
    var filteredUserViewModels: [UserViewModel] = [UserViewModel]()
    var hasNetwork = true
    let spinner = CustomProgressView()
    var since = 0
    var per_page = 10
    var canReload = false
    var lastIndexPath:IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupSearchBar()
        self.setupLabelNetwork()
        self.setupTableView()
        
        self.setupView()
        self.setupReachability()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        self.tblVwUsers.reloadData()
    }
    
    func setupSearchBar(){
        self.searchBar.delegate = self
        self.view.addSubview(self.searchBar)
        self.searchBar.setCustomConstraint()
        
    }
    
    func setupLabelNetwork(){
        self.lblNetworkStatus.backgroundColor = UIColor.red
        self.lblNetworkStatus.textColor = UIColor.white
        self.lblNetworkStatus.text = "No Internet"
        self.lblNetworkStatus.textAlignment = .center
        self.lblNetworkStatus.isHidden = true
        self.view.addSubview(self.lblNetworkStatus)
        
        self.lblNetworkStatus.setCustomConstraint()
    }
    
    func setupTableView(){
        self.tblVwUsers.register(UserCell.self, forCellReuseIdentifier: "UserCell")
        self.tblVwUsers.rowHeight = 110
        self.tblVwUsers.delegate = self
        self.tblVwUsers.dataSource = self
        self.view.addSubview(self.tblVwUsers)
        self.tblVwUsers.setCustomConstraint(myTopAnchor: self.searchBar.bottomAnchor, myBottomAnchor: self.lblNetworkStatus.topAnchor)
        
    }
    
    func fetchData(since: Int, per_page: Int, message:String = "Please wait..."){

        self.spinner.showActivityIndicator(self.view, message: message, theme: self.traitCollection.userInterfaceStyle == .dark ? "dark" : "light")
        Service.shared.fetchUsers(since: since, per_page: per_page) { (users, err) in
            self.spinner.hideActivityIndicator(self.view)
            if let err = err {
                self.tblVwUsers.isUserInteractionEnabled = true
                print("Failed to fetch users:", err)
                return
            }
            
            for user in users ?? [] {
                
                let filtered = self.userViewModels.filter { $0.login == user.login }
                if(filtered.count == 0){
                    self.userViewModels.append(UserViewModel(user: user))
                    self.filteredUserViewModels.append(UserViewModel(user: user))
                }else{
                    print("fetch data from api has a duplicate. user = \(user.login!)")
                }
                
            }
            self.tblVwUsers.isUserInteractionEnabled = true
//            self.userViewModels = users?.map({return UserViewModel(user: $0)}) ?? []
//            self.filteredUserViewModels = users?.map({return UserViewModel(user: $0)}) ?? []
            self.tblVwUsers.reloadData()
        }
    }
    
    func fetchDataOffline(){
        self.userViewModels.removeAll()
        let results = CoreDataManager.shared.retrieveUsers()
        for data in results {
            self.userViewModels.append(UserViewModel(user: User.init(login: (data.value(forKey: "login") as! String), avatar_url: (data.value(forKey: "avatar_url") as! String), html_url: (data.value(forKey: "html_url") as! String))) )
            self.filteredUserViewModels.append(UserViewModel(user: User.init(login: (data.value(forKey: "login") as! String), avatar_url: (data.value(forKey: "avatar_url") as! String), html_url: (data.value(forKey: "html_url") as! String))) )
        }
        self.tblVwUsers.reloadData()
    }
    
    func setupReachability(){
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            NotificationCenter.default.addObserver( self, selector: #selector( self.reachabilityChanged ),name: Notification.Name.reachabilityChanged, object: reachability )
        }
    }
    
    func setupView(){
        self.searchBar.delegate = self
        self.title = "Github Users"
        NotificationCenter.default.addObserver(self, selector:#selector(self.networkChanged), name: Notification.Name("UsersControllerNetworkChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(self.reloadTableView), name: Notification.Name("UsersControllerReload"), object: nil)
    }
    
    @objc func reloadTableView(note: Notification) {
        if(self.hasNetwork){
            self.fetchData(since: self.since, per_page: 10)
        }else{
            self.fetchDataOffline()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredUserViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as! UserCell
        var userViewModel = filteredUserViewModels[indexPath.row]
        
        if((indexPath.row + 1) % 4 == 0){
            userViewModel.invertImage = true
        }
        
        cell.userViewModel = userViewModel
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        self.performSegue(withIdentifier: "UserProfileSegue", sender: self)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

            let lastItem = self.userViewModels.count - 1
            if(indexPath.row == lastItem){
                self.canReload = true
                self.lastIndexPath = indexPath
            }

    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView.contentOffset.y > 10.0 && self.canReload && self.hasNetwork {
            
            self.canReload = false
            self.fetchData(since: self.since + self.userViewModels.count, per_page: 10, message: "Loading more data...")
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "UserProfileSegue"
        {
            
            let userProfileController = segue.destination as! UserProfileController
            userProfileController.selectedUser = self.filteredUserViewModels[self.selectedIndex].login
            
            let hasSeen = CoreDataManager.shared.hasSeen(login: userProfileController.selectedUser)
            userProfileController.hasSeen = hasSeen
            userProfileController.hasNetwork = self.hasNetwork
            
            if((self.selectedIndex + 1) % 4 == 0){
                userProfileController.invertedImage = true
            }
            
            if(!hasSeen && hasNetwork){
                CoreDataManager.shared.markHasSeen(login:userProfileController.selectedUser)
            }
            
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let newSearchText = searchText.lowercased()
        if(newSearchText == ""){
            self.filteredUserViewModels = self.userViewModels
        }else{
            let filtered = self.filteredUserViewModels.filter { $0.login.contains(newSearchText) }
            self.filteredUserViewModels = filtered
        }
        self.tblVwUsers.reloadData()

    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.setShowsCancelButton(false, animated: true)
        self.searchBar.resignFirstResponder()
    }
    
    @objc func networkChanged(note: Notification) {
      let hasNetwork = note.object as! Bool
        if(hasNetwork){
            self.lblNetworkStatus.isHidden = true
        }else{
            self.lblNetworkStatus.isHidden = false
        }
    }
    
    @objc private func reachabilityChanged( notification: NSNotification )
    {
        guard let reachability = notification.object as? Reachability else
        {
            return
        }

        if reachability.connection != .unavailable
        {
            self.fetchData(since: self.since, per_page: 10)
            self.lblNetworkStatus.isHidden = true
            self.hasNetwork = true
            
            if reachability.connection == .wifi
            {
                print("Reachable via WiFi")
            }
            else
            {
                print("Reachable via Cellular")
            }
        }
        else
        {
            self.fetchDataOffline()
            print("Network not reachable")
            self.lblNetworkStatus.isHidden = false
            self.hasNetwork = false
        }
    }

}

//
//  UserViewModel.swift
//  TawkGithubUsers
//
//  Created by Luigi Guevarra on 7/27/20.
//  Copyright © 2020 Luigi Guevarra. All rights reserved.
//

import Foundation
import UIKit

struct UserViewModel {
    
    let login: String
    let html_url: String
    let avatar_url: String
//    let notes: String
    var invertImage: Bool = false
    
    init(user: User) {
        self.login = user.login ?? ""
        self.html_url = user.html_url ?? ""
        self.avatar_url = user.avatar_url ?? ""
//        self.notes = ""
    }
    
}

//
//  ConnectionManager.swift
//  TawkGithubUsers
//
//  Created by Luigi Guevarra on 7/29/20.
//  Copyright © 2020 Luigi Guevarra. All rights reserved.
//

import Foundation

class ConnectionManager {

static let sharedInstance = ConnectionManager()
private var reachability : Reachability!
    
func observeReachability(){
    self.reachability = try? Reachability()
    NotificationCenter.default.addObserver(self, selector:#selector(self.reachabilityChanged), name: NSNotification.Name.reachabilityChanged, object: nil)
    do {
        try self.reachability.startNotifier()
    }
    catch(let error) {
        print("Error occured while starting reachability notifications : \(error.localizedDescription)")
    }
}

@objc func reachabilityChanged(note: Notification) {
    let reachability = note.object as! Reachability
    switch reachability.connection {
    case .cellular:
        NotificationCenter.default.post(name: Notification.Name("UsersControllerNetworkChanged"), object: true)
        print("Network available via Cellular Data.")
        break
    case .wifi:
        NotificationCenter.default.post(name: Notification.Name("UsersControllerNetworkChanged"), object: true)
        print("Network available via WiFi.")
        break
    case .none:
        NotificationCenter.default.post(name: Notification.Name("UsersControllerNetworkChanged"), object: false)
        print("Network is not available.")
        break
    case .unavailable:
        NotificationCenter.default.post(name: Notification.Name("UsersControllerNetworkChanged"), object: false)
        print("Network is  unavailable.")
        break
    }
  }
}

//
//  User.swift
//  TawkGithubUsers
//
//  Created by Luigi Guevarra on 7/27/20.
//  Copyright © 2020 Luigi Guevarra. All rights reserved.
//

import Foundation


struct User: Codable{
    var login: String?
    var avatar_url: String?
    var html_url: String?
}

//
//  UserProfileController.swift
//  TawkGithubUsers
//
//  Created by Luigi Guevarra on 7/27/20.
//  Copyright © 2020 Luigi Guevarra. All rights reserved.
//

import UIKit

class UserProfileController: UIViewController {

    var selectedUser = ""
    var hasSeen = false
    var hasNetwork = true
    var invertedImage = false
    let spinner = CustomProgressView()
    
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lblFollowers: UILabel!
    @IBOutlet weak var lblFollowing: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var lblBlog: UILabel!
    @IBOutlet weak var txtNotes: UITextView!
    @IBOutlet weak var vwBasicInfo: UIView!
    @IBOutlet weak var btnSave: UIButton!
    
    var userProfileViewModel: UserProfileViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.traitCollection.userInterfaceStyle == .dark {
            self.setupViewForDarkMode()
        } else {
            self.setupViewForLightMode()
        }
        
        if(self.hasNetwork){
            self.fetchData()
        }else{
            if(self.hasSeen){
                self.fetchDataOffline()
            }else{
                self.setupViewForNoOfflineData()
            }
        }
        
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        
        if previousTraitCollection?.userInterfaceStyle == .dark {
            self.setupViewForLightMode()
        } else {
            self.setupViewForDarkMode()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.post(name: Notification.Name("UsersControllerReload"), object: nil)
    }
    
    func fetchData(){
        self.spinner.showActivityIndicator(self.view, message: "Please wait...", theme: self.traitCollection.userInterfaceStyle == .dark ? "dark" : "light")
        Service.shared.fetchUserProfile(selectedUser: self.selectedUser) { (userProfile, err) in
            self.spinner.hideActivityIndicator(self.view)
            if let err = err {
                print("Failed to fetch user profile:", err)
                return
            }
            self.userProfileViewModel = UserProfileViewModel(userProfile: userProfile!)
            self.imgAvatar.loadImage(login: (self.userProfileViewModel?.login)!, avatarUrlString: self.userProfileViewModel?.avatar_url ?? "", imgAvatar: self.imgAvatar, inverted: self.invertedImage)
            self.lblFollowers.text = "\(self.userProfileViewModel?.followers ?? 0)"
            self.lblFollowing.text = "\(self.userProfileViewModel?.following ?? 0)"
            self.lblName.text = "\(self.userProfileViewModel?.name ?? "")"
            self.lblCompany.text = "\(self.userProfileViewModel?.company ?? "")"
            self.lblBlog.text = "\(self.userProfileViewModel?.blog ?? "")"
            
            if(!self.hasSeen){
                CoreDataManager.shared.saveUserProfile(login:self.userProfileViewModel?.login ?? "", avatar_url:self.userProfileViewModel?.avatar_url ?? "", followers:self.userProfileViewModel?.followers ?? 0, following:self.userProfileViewModel?.following ?? 0, name:self.userProfileViewModel?.name ?? "", company:self.userProfileViewModel?.company ?? "", blog:self.userProfileViewModel?.blog ?? "")
            }else{
                self.txtNotes.text = CoreDataManager.shared.getNotes(login: self.selectedUser)
            }
            
            if self.traitCollection.userInterfaceStyle == .dark {
                self.setupViewForDarkMode()
            } else {
                self.setupViewForLightMode()
            }
        }
    }
    
    func fetchDataOffline(){
        
        let results = CoreDataManager.shared.retrieveUserProfile(login: self.selectedUser)
        
        if(results.count == 0){
            self.setupViewForNoOfflineData()
        }else{
            self.imgAvatar.loadImage(login: (results[0].value(forKey: "login") as? String)!, avatarUrlString: (results[0].value(forKey: "avatar_url") as? String)!, imgAvatar: self.imgAvatar, inverted: self.invertedImage)
            self.lblFollowers.text = "\(results[0].value(forKey: "followers") as! Int)"
            self.lblFollowing.text = "\(results[0].value(forKey: "following") as! Int)"
            self.lblName.text = results[0].value(forKey: "name") as? String
            self.lblCompany.text = results[0].value(forKey: "company") as? String
            self.lblBlog.text = results[0].value(forKey: "blog") as? String
            self.txtNotes.text = results[0].value(forKey: "notes") as? String
//            print(results[0].value(forKey: "followers"))
//            print(results[0].value(forKey: "followers") as? Int)
           if self.traitCollection.userInterfaceStyle == .dark {
                self.setupViewForDarkMode()
            } else {
                self.setupViewForLightMode()
            }
        }
        
        
        
        
    }
    
    func setupViewForLightMode(){
        self.title = self.userProfileViewModel?.name
        
        self.txtNotes.layer.borderColor = UIColor.black.cgColor
        self.txtNotes.layer.borderWidth = 2
        self.txtNotes.layer.cornerRadius = 5
        
        self.imgAvatar.layer.borderColor = UIColor.black.cgColor
        self.imgAvatar.layer.borderWidth = 2
        self.imgAvatar.layer.cornerRadius = 5
        
        self.vwBasicInfo.layer.borderColor = UIColor.black.cgColor
        self.vwBasicInfo.layer.borderWidth = 2
        self.vwBasicInfo.layer.cornerRadius = 5
        
        self.btnSave.layer.borderColor = UIColor.black.cgColor
        self.btnSave.setTitleColor(UIColor.black, for: .normal)
        
        self.btnSave.layer.borderWidth = 2
        self.btnSave.layer.cornerRadius = 5
    
    }
    
    func setupViewForDarkMode(){
        self.title = self.userProfileViewModel?.name
        
        self.txtNotes.layer.borderColor = UIColor.white.cgColor
        self.txtNotes.backgroundColor = UIColor.lightGray
        self.txtNotes.layer.borderWidth = 2
        self.txtNotes.layer.cornerRadius = 5
        
        self.imgAvatar.layer.borderColor = UIColor.white.cgColor
        self.imgAvatar.layer.borderWidth = 2
        self.imgAvatar.layer.cornerRadius = 5
        
        self.vwBasicInfo.layer.borderColor = UIColor.white.cgColor
        self.vwBasicInfo.layer.borderWidth = 2
        self.vwBasicInfo.layer.cornerRadius = 5
        
        self.btnSave.layer.borderColor = UIColor.white.cgColor
        self.btnSave.setTitleColor(UIColor.white, for: .normal)

        self.btnSave.layer.borderWidth = 2
        self.btnSave.layer.cornerRadius = 5
    
    }
    
    func setupViewForNoOfflineData(){
        
        if self.traitCollection.userInterfaceStyle == .dark {
            self.setupViewForDarkMode()
        } else {
            self.setupViewForLightMode()
        }
        
        self.title = "No Offline Data"
//        self.imgAvatar.backgroundColor = UIColor.white
        self.lblFollowers.text = "0"
        self.lblFollowing.text = "0"
        self.lblName.text = "N/A"
        self.lblCompany.text = "N/A"
        self.lblBlog.text = "N/A"
        self.txtNotes.text = "N/A"
    }
    
    @IBAction func btnSaveTapped(_ sender: Any) {
        
        let results = CoreDataManager.shared.retrieveUserProfile(login: self.selectedUser)
        
        if(results.count == 0){
            self.showAlert()
        }else{
            let value = self.txtNotes.text ?? ""
            let trimmedString = value.trimmingCharacters(in: .whitespaces)
            if(trimmedString == ""){
                CoreDataManager.shared.markHasNotes(login:self.selectedUser, has_notes: false)
            }else{
                CoreDataManager.shared.markHasNotes(login:self.selectedUser, has_notes: true)
                CoreDataManager.shared.saveNotes(login:self.selectedUser, notes:trimmedString)
            }
//            NotificationCenter.default.post(name: Notification.Name("UsersControllerReload"), object: nil)
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func showAlert(){
        let alert = UIAlertController(title: "Failed to Save Notes", message: "This user has no Profile saved locally. It has not been loaded before when network is available.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    

}

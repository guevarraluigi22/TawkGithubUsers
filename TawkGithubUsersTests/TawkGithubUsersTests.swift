//
//  TawkGithubUsersTests.swift
//  TawkGithubUsersTests
//
//  Created by Luigi Guevarra on 7/27/20.
//  Copyright © 2020 Luigi Guevarra. All rights reserved.
//

import XCTest
@testable import TawkGithubUsers

class TawkGithubUsersTests: XCTestCase {
    
    func testUserViewModel(){
        let user = User(login: "test_login", avatar_url: "test_avatar_url", html_url: "test_html_url")
        let userViewModel = UserViewModel(user: user)
        
        XCTAssertEqual(user.login, userViewModel.login)
        XCTAssertEqual(user.avatar_url, userViewModel.avatar_url)
        XCTAssertEqual(user.html_url, userViewModel.html_url)
    }
    
    func testUserProfileViewModel(){
        let userProfile = UserProfile(login: "test_login", avatar_url: "test_avatar_url", name: "test_name", company: "test_company", blog: "test_blog", followers: 0, following: 1)
        
        let userProfileViewModel = UserProfileViewModel(userProfile: userProfile)
        
        XCTAssertEqual(userProfile.login, userProfileViewModel.login)
        XCTAssertEqual(userProfile.avatar_url, userProfileViewModel.avatar_url)
        XCTAssertEqual(userProfile.name, userProfileViewModel.name)
        XCTAssertEqual(userProfile.company, userProfileViewModel.company)
        XCTAssertEqual(userProfile.blog, userProfileViewModel.blog)
        XCTAssertEqual(userProfile.followers, userProfileViewModel.followers)
        XCTAssertEqual(userProfile.following, userProfileViewModel.following)
    }
    

}

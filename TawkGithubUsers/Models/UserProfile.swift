//
//  UserProfile.swift
//  TawkGithubUsers
//
//  Created by Luigi Guevarra on 7/27/20.
//  Copyright © 2020 Luigi Guevarra. All rights reserved.
//

import Foundation

struct UserProfile: Codable{
    var login: String?
    var avatar_url: String?
    var name: String?
    var company: String?
    var blog: String?
    var followers: Int?
    var following: Int?
}

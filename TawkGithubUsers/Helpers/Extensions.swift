//
//  Extensions.swift
//  TawkGithubUsers
//
//  Created by Luigi Guevarra on 7/27/20.
//  Copyright © 2020 Luigi Guevarra. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView{
    
    func convertImageToBase64String (img: UIImage) -> String {
        return img.jpegData(compressionQuality: 0.5)?.base64EncodedString() ?? ""
    }
    
    func convertBase64StringToImage (imageBase64String:String) -> UIImage? {
        if let imageData = Data.init(base64Encoded: imageBase64String, options: .init(rawValue: 0)){
            if let image = UIImage(data: imageData) {
                return image
            }
        }
        return nil
    }
    
    func loadImage(login:String, avatarUrlString: String, imgAvatar: UIImageView, inverted: Bool = false){
        
        let base64Image = CoreDataManager.shared.getBase64Image(login: login)
        
        if(base64Image != ""){
            if let image = self.convertBase64StringToImage(imageBase64String: base64Image){
                if(inverted){
                    imgAvatar.image = image.invertedImage()
                }else{
                    imgAvatar.image = image
                }
                return
            }else{
                return
            }
        }
        
        
        let avatarUrl =  URL(string: avatarUrlString ) ?? URL(string: "")
        
        DispatchQueue.global().async {
            if let data = try? Data(contentsOf: avatarUrl!){
                if let image = UIImage(data: data){
                    let base64_image = self.convertImageToBase64String(img: image)
                    
                    DispatchQueue.main.async {
                        CoreDataManager.shared.storeBase64Image(login: login, base64_image: base64_image)
                        if(inverted){
                            imgAvatar.image = image.invertedImage()
                        }else{
                            imgAvatar.image = image
                        }
                    }
                }
            }
        }
    }
    
    
    
    
}

extension UIImage {
    func invertedImage() -> UIImage? {
        guard let cgImage = self.cgImage else { return nil }
        let ciImage = CoreImage.CIImage(cgImage: cgImage)
        guard let filter = CIFilter(name: "CIColorInvert") else { return nil }
        filter.setDefaults()
        filter.setValue(ciImage, forKey: kCIInputImageKey)
        let context = CIContext(options: nil)
        guard let outputImage = filter.outputImage else { return nil }
        guard let outputImageCopy = context.createCGImage(outputImage, from: outputImage.extent) else { return nil }
        return UIImage(cgImage: outputImageCopy, scale: self.scale, orientation: .up)
    }
}

extension UITableView{

    func indicatorView() -> UIActivityIndicatorView{
        var activityIndicatorView = UIActivityIndicatorView()
        if self.tableFooterView == nil{
            let indicatorFrame = CGRect(x: 0, y: 0, width: self.bounds.width, height: 40)
            activityIndicatorView = UIActivityIndicatorView(frame: indicatorFrame)
            activityIndicatorView.isHidden = false
            activityIndicatorView.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin]
            activityIndicatorView.isHidden = true
            self.tableFooterView = activityIndicatorView
            return activityIndicatorView
        }else{
            return activityIndicatorView
        }
    }

    func addLoading(_ indexPath:IndexPath, closure: @escaping (() -> Void)){
        indicatorView().startAnimating()
        if let lastVisibleIndexPath = self.indexPathsForVisibleRows?.last {
            if indexPath == lastVisibleIndexPath && indexPath.row == self.numberOfRows(inSection: 0) - 1 {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    closure()
                }
            }
        }
        indicatorView().isHidden = false
    }

    func stopLoading(){
        indicatorView().stopAnimating()
        indicatorView().isHidden = true
    }
}


extension UISearchBar {
    func setCustomConstraint() {
        guard let superview = superview else { return }
        translatesAutoresizingMaskIntoConstraints = false

        topAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.topAnchor).isActive = true
        heightAnchor.constraint(equalToConstant: 50).isActive = true
        leftAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.leftAnchor).isActive = true
        rightAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.rightAnchor).isActive = true
    }
}

extension UIImageView {
    func setAvatarConstraint() {
        guard let superview = superview else { return }
        translatesAutoresizingMaskIntoConstraints = false

//        topAnchor.constraint(greaterThanOrEqualTo: superview.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
        centerYAnchor.constraint(equalTo: superview.centerYAnchor).isActive = true
        leadingAnchor.constraint(greaterThanOrEqualTo: superview.safeAreaLayoutGuide.leadingAnchor, constant: 10).isActive = true
        heightAnchor.constraint(equalToConstant: 90).isActive = true
        widthAnchor.constraint(equalToConstant: 90).isActive = true

    }
    
    func setNoteConstraint() {
        guard let superview = superview else { return }
        translatesAutoresizingMaskIntoConstraints = false

//        topAnchor.constraint(greaterThanOrEqualTo: superview.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
        trailingAnchor.constraint(greaterThanOrEqualTo: superview.safeAreaLayoutGuide.trailingAnchor, constant: -10).isActive = true
        heightAnchor.constraint(equalToConstant: 45).isActive = true
        widthAnchor.constraint(equalToConstant: 45).isActive = true
        centerYAnchor.constraint(equalTo: superview.centerYAnchor).isActive = true
    }
}

extension UIStackView {
    func setCustomConstraint(leading:NSLayoutXAxisAnchor) {
        guard let superview = superview else { return }
        translatesAutoresizingMaskIntoConstraints = false

        leadingAnchor.constraint(greaterThanOrEqualTo: leading, constant: 10).isActive = true
        centerYAnchor.constraint(equalTo: superview.centerYAnchor).isActive = true
    }
}

extension UITableView {
    func setCustomConstraint(myTopAnchor:NSLayoutYAxisAnchor, myBottomAnchor:NSLayoutYAxisAnchor) {
        guard let superview = superview else { return }
        translatesAutoresizingMaskIntoConstraints = false

        topAnchor.constraint(equalTo: myTopAnchor).isActive = true
        bottomAnchor.constraint(equalTo: myBottomAnchor).isActive = true
        leftAnchor.constraint(equalTo: superview.leftAnchor).isActive = true
        rightAnchor.constraint(equalTo: superview.rightAnchor).isActive = true
    }
}

extension UILabel {
    func setCustomConstraint() {
        guard let superview = superview else { return }
        translatesAutoresizingMaskIntoConstraints = false
        
        heightAnchor.constraint(equalToConstant: 25).isActive = true
        leftAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.leftAnchor).isActive = true
        bottomAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.bottomAnchor).isActive = true
        rightAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.rightAnchor).isActive = true
    }
}

extension UIView {
    func setSeenConstraint() {
        guard let superview = superview else { return }
        translatesAutoresizingMaskIntoConstraints = false
        
        topAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.topAnchor).isActive = true
        leftAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.leftAnchor).isActive = true
        bottomAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.bottomAnchor).isActive = true
        rightAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.rightAnchor).isActive = true
    }
}

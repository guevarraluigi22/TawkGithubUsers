//
//  CustomProgressView.swift
//  TawkGithubUsers
//
//  Created by Luigi Guevarra on 8/1/20.
//  Copyright © 2020 Luigi Guevarra. All rights reserved.
//

import UIKit
import Foundation

class CustomProgressView {
    
    var container: UIView = UIView()
    var loadingView: UIView = UIView()
    var logo: UIImageView = UIImageView()
    var lblmessage: UILabel = UILabel()
    
    func showActivityIndicator(_ uiView: UIView, message: String!, theme:String = "light") {
        
        container.isUserInteractionEnabled = true
        container.frame = uiView.frame
        container.center = uiView.center
        container.backgroundColor = UIColorFromHex(0x25313E, alpha: 0.2)
        
        //container
        loadingView.frame = CGRect(x: 0, y: 0, width: 185, height: 140)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColorFromHex(0x25313E, alpha: 0.97)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 5
        
        //logo
        logo.frame = CGRect(x: 0.0, y: 0.0, width: 58.0, height: 58.0)
        logo.image = UIImage(named:"wait_light")!
        logo.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 3)
        //logo animation
        let pulseAnimation = CABasicAnimation(keyPath: "opacity")
        pulseAnimation.duration = 1.0
        pulseAnimation.fromValue = 1
        pulseAnimation.toValue = 0.3
        pulseAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        pulseAnimation.autoreverses = true
        pulseAnimation.repeatCount = Float.greatestFiniteMagnitude
        logo.layer.add(pulseAnimation, forKey: nil)
        
        //message
        if message .isEmpty {
            lblmessage.text = "Processing Request"
        } else {
            lblmessage.text = message
        }
        lblmessage.textColor = UIColor.white
        lblmessage.frame = CGRect(x: 0.0, y: 0.0, width: 160.0, height: 30)
        lblmessage.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 1.3)
        lblmessage.textAlignment = NSTextAlignment.center
        lblmessage.backgroundColor = UIColor.clear
        lblmessage.font = lblmessage.font.withSize(13.0)
        
        lblmessage.font = UIFont(name: "Arial", size: 12.0)
        lblmessage.font = UIFont.boldSystemFont(ofSize: 12)
        
        loadingView.addSubview(logo)
        loadingView.addSubview(lblmessage)
        container.addSubview(loadingView)
        container.tag = 999
        
        
        if(theme == "dark"){
            logo.image = UIImage(named:"wait")!
            loadingView.backgroundColor = UIColor.white
            lblmessage.textColor = UIColor.black
        }
        
        uiView.addSubview(container)
        return
    }
    
    func hideActivityIndicator(_ uiView: UIView) {
        container.removeFromSuperview()
    }
    
    
    func UIColorFromHex(_ rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }

}

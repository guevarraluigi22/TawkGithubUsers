//
//  UserProfileViewModel.swift
//  TawkGithubUsers
//
//  Created by Luigi Guevarra on 7/27/20.
//  Copyright © 2020 Luigi Guevarra. All rights reserved.
//

import Foundation
import UIKit

struct UserProfileViewModel {
    
    let login: String?
    let avatar_url: String?
    let name: String?
    let company: String?
    let blog: String?
    let followers: Int?
    let following: Int?
    
    init(userProfile: UserProfile) {
        
        self.login = userProfile.login ?? ""
        self.avatar_url = userProfile.avatar_url ?? ""
        self.name = userProfile.name ?? ""
        self.company = userProfile.company ?? ""
        self.blog = userProfile.blog ?? ""
        self.followers = userProfile.followers ?? 0
        self.following = userProfile.following ?? 0
        

        
        
    }
    
}

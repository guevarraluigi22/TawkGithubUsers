//
//  CoreDataManager.swift
//  TawkGithubUsers
//
//  Created by Luigi Guevarra on 7/28/20.
//  Copyright © 2020 Luigi Guevarra. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class CoreDataManager {
    static let shared = CoreDataManager()
    
    //USERS
    func saveUser(login:String, html_url:String, avatar_url:String){
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "Users")
        fetchRequest.predicate = NSPredicate(format: "login = %@", login)
         do
         {
             let test = try managedContext.fetch(fetchRequest)
    
            if(test.count > 0){
             //UPDATE
                let objectUpdate = test[0] as! NSManagedObject
                
                    objectUpdate.setValue(login, forKey: "login")
                    objectUpdate.setValue(login, forKey: "html_url")
                    objectUpdate.setValue(login, forKey: "avatar_url")
                
                    do{
                        try managedContext.save()
                    }
                    catch
                    {
                        print("Failed to add notes")
                    }
                
            }else{
            //CREATE
                
                let userEntity = NSEntityDescription.entity(forEntityName: "Users", in: managedContext)!
                
                let user = NSManagedObject(entity: userEntity, insertInto: managedContext)
                user.setValue(login, forKeyPath: "login")
                user.setValue(html_url, forKeyPath: "html_url")
                user.setValue(avatar_url, forKeyPath: "avatar_url")

                do {
                    try managedContext.save()
                   
                } catch let error as NSError {
                    print("Could not save. \(error), \(error.userInfo)")
                }
                
            }
            
                 
        }
        catch
        {
            print("Failed to fetch user before saving")
        }
        
        
        
    }
    
    func retrieveUsers(login:String = "")->[NSManagedObject] {
            
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return [NSManagedObject]() }
            
            let managedContext = appDelegate.persistentContainer.viewContext
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        
            if(login != ""){
                fetchRequest.predicate = NSPredicate(format: "login = %@", login)
            }
            
            do {
                let result = try managedContext.fetch(fetchRequest)
                return result as! [NSManagedObject]
                
            } catch {
                print("Failed to retrieve users")
                return [NSManagedObject]()
            }
    }
    
    func storeBase64Image(login:String, base64_image:String){
     
         guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
         
         let managedContext = appDelegate.persistentContainer.viewContext
         
         let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "Users")
         fetchRequest.predicate = NSPredicate(format: "login = %@", login)
         do
         {
             let test = try managedContext.fetch(fetchRequest)
    
                 let objectUpdate = test[0] as! NSManagedObject
                 objectUpdate.setValue(base64_image, forKey: "base64_image")
                 do{
                     try managedContext.save()
                 }
                 catch
                 {
                     print("Failed to store image")
                 }
             }
         catch
         {
             print("Failed to store image")
         }
    
     }
    
    func getBase64Image(login:String = "")->String {
            
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return "" }
            
            let managedContext = appDelegate.persistentContainer.viewContext
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        
            if(login != ""){
                fetchRequest.predicate = NSPredicate(format: "login = %@", login)
            }
            
            do {
                let result = try managedContext.fetch(fetchRequest)
                for data in result as! [NSManagedObject] {
                    return data.value(forKey: "base64_image") as! String
                }
                
            } catch {
                
                print("Failed to retrieve user image")
            }
            return ""
    }
    
    func markHasSeen(login:String){
     
         guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
         
         let managedContext = appDelegate.persistentContainer.viewContext
         
         let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "Users")
         fetchRequest.predicate = NSPredicate(format: "login = %@", login)
         do
         {
             let test = try managedContext.fetch(fetchRequest)
    
                 let objectUpdate = test[0] as! NSManagedObject
                 objectUpdate.setValue(true, forKey: "has_seen")
                 do{
                     try managedContext.save()
                 }
                 catch
                 {
                     print("Failed to update user")
                 }
             }
         catch
         {
             print("Failed to update users")
         }
    
     }
    
    func markHasNotes(login:String, has_notes: Bool){
     
         guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
         
         let managedContext = appDelegate.persistentContainer.viewContext
         
         let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "Users")
         fetchRequest.predicate = NSPredicate(format: "login = %@", login)
         do
         {
             let test = try managedContext.fetch(fetchRequest)
    
                 let objectUpdate = test[0] as! NSManagedObject
                 objectUpdate.setValue(has_notes, forKey: "has_notes")
                 do{
                     try managedContext.save()
                 }
                 catch
                 {
                     print("Failed to update user")
                 }
             }
         catch
         {
             print("Failed to update users")
         }
    
     }
    
    func hasSeen(login:String = "")->Bool {
            
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return false }
            
            let managedContext = appDelegate.persistentContainer.viewContext
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        
            if(login != ""){
                fetchRequest.predicate = NSPredicate(format: "login = %@", login)
            }
            
            do {
                let result = try managedContext.fetch(fetchRequest)
                for data in result as! [NSManagedObject] {
//                    print(data.value(forKey: "has_seen") as! Bool)
                    return data.value(forKey: "has_seen") as! Bool
                }
                
            } catch {
                
                print("Failed to retrieve users")
            }
            return false
        }
    
    //USER PROFILES
    
    func retrieveUserProfile(login:String = "")->[NSManagedObject] {
            
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return [NSManagedObject]() }
            
            let managedContext = appDelegate.persistentContainer.viewContext
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "UserProfiles")
        
            if(login != ""){
                fetchRequest.predicate = NSPredicate(format: "login = %@", login)
            }
            
            do {
                let result = try managedContext.fetch(fetchRequest)
                return result as! [NSManagedObject]
                
            } catch {
                print("Failed to retrieve user profiles")
                return [NSManagedObject]()
            }
    }
    
    func saveUserProfile(login:String, avatar_url:String, followers:Int, following:Int, name:String, company:String, blog:String){
            
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
    
            let managedContext = appDelegate.persistentContainer.viewContext
            
            let userEntity = NSEntityDescription.entity(forEntityName: "UserProfiles", in: managedContext)!
            
            let userProfile = NSManagedObject(entity: userEntity, insertInto: managedContext)
            userProfile.setValue(login, forKeyPath: "login")
            userProfile.setValue(avatar_url, forKeyPath: "avatar_url")
            userProfile.setValue(followers, forKeyPath: "followers")
            userProfile.setValue(following, forKeyPath: "following")
            userProfile.setValue(name, forKeyPath: "name")
            userProfile.setValue(company, forKeyPath: "company")
            userProfile.setValue(blog, forKeyPath: "blog")

            do {
                try managedContext.save()
               
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    
    func saveNotes(login:String, notes:String){
     
         guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
         
         let managedContext = appDelegate.persistentContainer.viewContext
         
         let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "UserProfiles")
         fetchRequest.predicate = NSPredicate(format: "login = %@", login)
         do
         {
             let test = try managedContext.fetch(fetchRequest)
    
                 let objectUpdate = test[0] as! NSManagedObject
                 objectUpdate.setValue(notes, forKey: "notes")
                 do{
                     try managedContext.save()
                 }
                 catch
                 {
                     print("Failed to add notes")
                 }
             }
         catch
         {
             print("Failed to add notes")
         }
    
     }
    
    func getNotes(login:String = "")->String {
            
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return "" }
            
            let managedContext = appDelegate.persistentContainer.viewContext
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "UserProfiles")
        
            if(login != ""){
                fetchRequest.predicate = NSPredicate(format: "login = %@", login)
            }
            
            do {
                let result = try managedContext.fetch(fetchRequest)
                for data in result as! [NSManagedObject] {
                    return data.value(forKey: "notes") as! String
                }
                
            } catch {
                
                print("Failed to retrieve user profile")
            }
            return ""
    }
    
}

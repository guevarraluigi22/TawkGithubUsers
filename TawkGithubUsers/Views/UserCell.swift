//
//  UserCell.swift
//  TawkGithubUsers
//
//  Created by Luigi Guevarra on 7/27/20.
//  Copyright © 2020 Luigi Guevarra. All rights reserved.
//

import Foundation
import UIKit

class UserCell: UITableViewCell {
    
    var imgAvatar = UIImageView()
    var lblUsername = UILabel()
    var lblDetail = UILabel()
    var imgNoteIndicator = UIImageView()
    var vwSeen = UIView()
    var stackVw = UIStackView()
    
    
    var userViewModel: UserViewModel! {
        didSet {
            let hasSeen = CoreDataManager.shared.hasSeen(login: userViewModel.login)
            if(hasSeen){
//                self.layer.backgroundColor = UIColor.systemGray5.cgColor
                self.vwSeen.isHidden = false
            }else{
                self.vwSeen.isHidden = true
            }
            
            CoreDataManager.shared.saveUser(login: userViewModel.login, html_url: userViewModel.html_url, avatar_url: userViewModel.html_url)
            
            self.lblUsername.text = userViewModel.login
            self.lblDetail.text = userViewModel.html_url

//            let avatar_url =  URL(string: self.userViewModel.avatar_url ) ?? URL(string: "")

            let notes = CoreDataManager.shared.getNotes(login: userViewModel.login)
            
            if(notes != ""){
                imgNoteIndicator.isHidden = false
            }else{
                imgNoteIndicator.isHidden = true
            }
            
            self.imgAvatar.loadImage(login: userViewModel.login, avatarUrlString: self.userViewModel.avatar_url, imgAvatar: self.imgAvatar, inverted: userViewModel.invertImage)
            
            self.UpdateCellAppearance()
            
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        
        self.vwSeen.backgroundColor = UIColor.systemGray5
        self.addSubview(self.vwSeen)
        self.vwSeen.setSeenConstraint()
        
        self.imgAvatar.layer.borderWidth = 2
        self.imgAvatar.layer.masksToBounds = false
//        self.imgAvatar.layer.borderColor = UIColor.black.cgColor
        self.imgAvatar.clipsToBounds = true
        
        self.imgAvatar.frame = CGRect(x: 0, y: 0, width: 90, height: 90)
//        self.imgAvatar.layer.cornerRadius = 45
        self.addSubview(self.imgAvatar)
        self.imgAvatar.setAvatarConstraint()
        
        self.imgNoteIndicator.frame = CGRect(x: 0, y: 0, width: 45, height: 45)
        self.addSubview(self.imgNoteIndicator)
        self.imgNoteIndicator.setNoteConstraint()
        
        self.stackVw.axis = .vertical
        self.stackVw.alignment = .fill
        self.stackVw.distribution = .fillEqually
        self.stackVw.spacing = 10.0
        
//        self.addSubview(self.lblUsername)
        
        self.lblUsername.font = .boldSystemFont(ofSize: 15.0)
        
        
        self.lblDetail.font = .systemFont(ofSize: 15)
        
        self.stackVw.addArrangedSubview(self.lblUsername)
        self.stackVw.addArrangedSubview(self.lblDetail)
        
        self.addSubview(self.stackVw)
        self.stackVw.setCustomConstraint(leading: self.imgAvatar.trailingAnchor)
        
        
        
    }
    
    func UpdateCellAppearance(){
        
        if self.traitCollection.userInterfaceStyle == .dark {
            self.imgNoteIndicator.image = UIImage(named: "note_dark")
            self.imgAvatar.layer.borderColor = UIColor.white.cgColor
            self.lblUsername.textColor = UIColor.white
            self.lblDetail.textColor = UIColor.lightGray
        }else{
            self.imgNoteIndicator.image = UIImage(named: "note_light")
            self.imgAvatar.layer.borderColor = UIColor.black.cgColor
            self.lblUsername.textColor = UIColor.black
            self.lblDetail.textColor = UIColor.darkGray
        }
        
        self.imgAvatar.layer.cornerRadius = 45
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    
    
}

//
//  Service.swift
//  TawkGithubUsers
//
//  Created by Luigi Guevarra on 7/27/20.
//  Copyright © 2020 Luigi Guevarra. All rights reserved.
//

import Foundation

class Service: NSObject {
    static let shared = Service()
    
    func fetchUsers(since: Int, per_page: Int, completion: @escaping ([User]?, Error?) -> ()) {
        
        guard let url = URL(string: apiUrl + "users?since=\(since)&per_page=\(per_page)") else { return }
        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            if let err = err {
                completion(nil, err)
                print("Failed to fetch users:", err)
                return
            }
            
            // check response
            guard let data = data else { return }
            do {
                let users = try JSONDecoder().decode([User].self, from: data)
                DispatchQueue.main.async {
                    completion(users, nil)
                }
            } catch let jsonErr {
                print("Failed to decode:", jsonErr)
            }
            }.resume()
    }
    
    func fetchUserProfile(selectedUser:String, completion: @escaping (UserProfile?, Error?) -> ()) {
        
        guard let url = URL(string: apiUrl + "users/" + selectedUser) else { return }
        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            if let err = err {
                completion(nil, err)
                print("Failed to fetch users:", err)
                return
            }
            
            // check response
            guard let data = data else { return }
            do {
                let userProfile = try JSONDecoder().decode(UserProfile.self, from: data)
                DispatchQueue.main.async {
                    completion(userProfile, nil)
                }
            } catch let jsonErr {
                print("Failed to decode:", jsonErr)
            }
            }.resume()
    }
    
}
